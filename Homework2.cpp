﻿// Homework2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>

int main()
{
    std::cout << "Hello World!\n";
    std::cout << "Enter your word!\n";
    std::string word;
    std::getline (std::cin,word);
    std::cout << word.length() <<"\n";
    std::cout << word.front() << "\n";
    std::cout << word.back() << "\n";
    return 0;
}


